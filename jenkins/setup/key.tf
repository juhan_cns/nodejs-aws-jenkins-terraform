resource "aws_key_pair" "s2sKeypair" {
  key_name = "s2sKeypair"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
  lifecycle {
    ignore_changes = ["public_key"]
  }
}
