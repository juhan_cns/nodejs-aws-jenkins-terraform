terraform {
  backend "s3" {
    bucket = "s2s-juhan-node-aws-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}
