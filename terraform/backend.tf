terraform {
  backend "s3" {
    bucket = "s2s-juhan-node-aws-jenkins-terraform"
    key = "node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}
